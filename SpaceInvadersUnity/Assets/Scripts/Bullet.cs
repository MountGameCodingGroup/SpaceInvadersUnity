﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    private int speed;
    private GameManager gamemanager;
    private Vector3 positionOriginal;

    // Use this for initialization
    void Start()
    {
        positionOriginal = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        gamemanager = GameObject.Find("World").GetComponent<GameManager>();
        Reset();
    }

    public void Reset()
    {
        transform.position = new Vector3(positionOriginal.x, positionOriginal.y, positionOriginal.z);
        gameObject.SetActive(false);
        speed = 0;
    }

    // Update is called once per frame
    void Update () {
        transform.position = new Vector3(transform.position.x, transform.position.y + (speed * Time.deltaTime), transform.position.z);
        CheckBounds();
    }

    public void Fire(Vector3 newPosition)
    {
        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
            transform.position = newPosition;
            speed = 7;
        }
    }

    void CheckBounds()
    {
        float objectsWidthDiv2 = gameObject.GetComponent<SpriteRenderer>().bounds.size.x / 2;
        float objectsHeightDiv2 = gameObject.GetComponent<SpriteRenderer>().bounds.size.y / 2;
        if (transform.position.y - objectsHeightDiv2 > 5)
        {
            Reset();
        }       
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Enemy")
        {
            Reset();
            gamemanager.GainPoint();
        }
    }
}
