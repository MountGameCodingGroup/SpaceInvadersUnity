﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private int speed;
    public Bullet bullet;
    private GameManager gamemanager;
    private Vector3 positionOriginal;

    // Use this for initialization
    void Start()
    {
        positionOriginal = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        gamemanager = GameObject.Find("World").GetComponent<GameManager>();
        Reset();
    }

    public void Reset()
    {
        transform.position = new Vector3(positionOriginal.x, positionOriginal.y, positionOriginal.z);
        speed = 0;
        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
        HandleInput();
        transform.position = new Vector3(transform.position.x + (speed * Time.deltaTime), transform.position.y, transform.position.z);
        CheckBounds();
    }

    void HandleInput()
    {
        if (Input.GetAxis("Horizontal") < 0)
        {
            speed = -7;
        }
        else if (Input.GetAxis("Horizontal") > 0)
        {
            speed = 7;
        }
        else
        {
            speed = 0;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            bullet.Fire(transform.position);
        }
    }

    void CheckBounds()
    {
        float objectsWidthDiv2 = gameObject.GetComponent<SpriteRenderer>().bounds.size.x / 2;
        float objectsHeight = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        if (transform.position.x + objectsWidthDiv2 > 8)
        {
            transform.position = new Vector3(8 - objectsWidthDiv2, transform.position.y, transform.position.z);
        }
        else if (transform.position.x - objectsWidthDiv2 < -8)
        {
            transform.position = new Vector3(-8 + objectsWidthDiv2, transform.position.y, transform.position.z);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Enemy")
        {
            gameObject.SetActive(false);
        }
    }
}
