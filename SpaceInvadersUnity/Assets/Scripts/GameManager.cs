﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Text points;
    public RectTransform gameover;
    private int score;
    public Enemy enemy;
    public Player player;
    public Bullet bullet;

    // Use this for initialization
    void Start () {
        Reset();
    }

    void Reset()
    {
        score = 0;
        UpdateUI();
        gameover.gameObject.SetActive(false);
        enemy.Reset();
        player.Reset();
        bullet.Reset();
    }

    // Update is called once per frame
    void Update () {
        CheckGameOver();
        HandleInput();
    }

    void HandleInput()
    {       
        if (Input.GetButtonDown("Submit") && gameover.gameObject.activeSelf)
        {
            Reset();
        }
    }

    void CheckGameOver()
    {
        if (score == 1)
        {
            SetGameOver();
        }
    }

    public void SetGameOver()
    {
        gameover.gameObject.SetActive(true);
    }

    public void GainPoint()
    {
        score++;
        UpdateUI();
    }

    void UpdateUI()
    {
        points.text = "" + score;
    }
}
