﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    private int speed;
    private GameManager gamemanager;
    private Vector3 positionOriginal;

    // Use this for initialization
    void Start () {
        positionOriginal = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        gamemanager = GameObject.Find("World").GetComponent<GameManager>();
        Reset();
    }

    public void Reset()
    {
        transform.position = new Vector3(positionOriginal.x, positionOriginal.y, positionOriginal.z);
        speed = 4;
        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
        transform.position = new Vector3(transform.position.x + (speed * Time.deltaTime), transform.position.y, transform.position.z);
        CheckBounds();
    }

    void CheckBounds()
    {
        float objectsWidthDiv2 = gameObject.GetComponent<SpriteRenderer>().bounds.size.x/2;
        float objectsHeight = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        if (transform.position.x + objectsWidthDiv2 > 8)
        {
            speed *= -1;
            transform.position = new Vector3(8 - objectsWidthDiv2, transform.position.y - objectsHeight, transform.position.z);
        }
        else if (transform.position.x - objectsWidthDiv2 < -8)
        {
            speed *= -1;
            transform.position = new Vector3(-8 + objectsWidthDiv2, transform.position.y - objectsHeight, transform.position.z);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Bullet" || collision.gameObject.name == "Player")
        {
            gameObject.SetActive(false);
        }
    }
}
